import React, { Component } from 'react';
import axios from "axios";
import PETS from './addPet';
import {Container, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
class main extends Component {

    state = {
        users: [],
        pets: [],
        owners: [],
      }
    
      componentDidMount = () => {
        axios.get("/api/users")
          .then(res => {
            const users = res.data;
            this.setState({ users });
          });


          axios.get("/api/pets")
          .then(res => {
            const pets = res.data;
            this.setState({ pets });
          });


          axios.get("/api/owners")
          .then(res => {
            const owners = res.data;
            this.setState({ owners });
          });

      }



    render() {
        const users = this.state.users;
        const pets = this.state.pets;
        const owners = this.state.owners;
        return (
          <div>
                <PETS />
                <Container>
                <Row className="masterRow">
                    <Col sm>
                        <h4>Users</h4>
                       {Object.keys(users).map((t) => <Row className="userRow">{users[t].name+" "+users[t].last}</Row>)}
                    </Col>
                    <Col sm>
                        <h4>Pets</h4>
                        {Object.keys(pets).map((t) => <Row className="petRow">{pets[t].name+" / "+pets[t].species}</Row>)} 
                       
                    </Col>
                    <Col sm>
                        <h4>Owners / Pets</h4>
                        {Object.keys(owners).map((t) => <Row className="ownerRow">{owners[t].name+" OWNS /"}{owners[t].pets.map(s => (<p>{" "+s.name}</p>))}</Row>)}
                    </Col>
                </Row>
                </Container>
          </div>
        );
    }
}

export default main;