# React.js frontend project (The backend is managed by [Express Project](https://gitlab.com/p156069/express.js-backend-project))




# Mobile View

![](https://i.ibb.co/bmgWZjY/Screenshot-9.png)

# Desktop View

![](https://i.ibb.co/Qmkdy12/Screenshot-10.png)


# API requests using AXIOS

![](https://i.ibb.co/1TN6Fgg/Screenshot-7.png)
