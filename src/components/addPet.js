import React, { Component } from 'react';
import axios from 'axios';

class addPet extends Component {

    state ={
        name: "",
        species:""
    }

    nameChangeHandler = (e) =>{
        this.setState({name: e.target.value});
    }
    specieChangeHandler = (e) =>{
      this.setState({species: e.target.value});
  }

    addPet = (e) =>{
      e.preventDefault();
      const data = this.state;
      axios.post("/api/pets", data)
      .then(res=>{
          console.log(res);
      })
      .catch(error=>{
          console.log(error);
      })
    }

    render() {
        const {name, species} = this.state;
        return (
            <div>
            <h3 style={{marginTop:'15px'}}>Pets & Owners Task</h3>
                <form onSubmit={this.addPet}>  
                <h4>Add Pet</h4>          
                <label htmlFor="name">Name</label>   
                <input 
                    type="text" 
                    name="name" 
                    type={name}
                    onChange={this.nameChangeHandler} 
                    /> 
                <label htmlFor="specie">Specie</label> 
                <input 
                    type="text" 
                    name="specie" 
                    type={species} 
                    onChange={this.specieChangeHandler}     
                    />
                <button type="submit">Add New Pet</button>
                </form>                
            </div>
        );
    }
}

export default addPet;